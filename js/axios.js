const serverUri = 'http://localhost:8080';


const fetchToDos = () => () => JSON.parse(localStorage.getItem('toDos'));
const createToDo = ({title, description, parent_task_id}) =>  () => {
    const toDosCurrent = JSON.parse(localStorage.getItem('toDos'));

    const addingItem = {
        id: toDosCurrent.length + 1,
        title,
        description,
        creator: 'user 1',
        parent_task_id
    };
    const toDos = [
        ...toDosCurrent,
        addingItem
    ];
    localStorage.setItem('toDos', JSON.stringify(toDos));
    return addingItem;

};

const deleteSelectedToDos = ({selectedIds}) => () => {
    const toDosCurrent = JSON.parse(localStorage.getItem('toDos'));
    const toDos = toDosCurrent.filter(toDo => selectedIds.indexOf(parseInt(toDo.id)) === -1);
    localStorage.setItem('toDos', JSON.stringify(toDos));
};

const getResolver = ({method, url, data}) => {
    if (method === 'get' && url === '/') {
        return fetchToDos();
    }

    if (method === 'post' && url === '/') {
        return createToDo(data);
    }

    if (method === 'delete' && url === '/') {
        return deleteSelectedToDos(data);
    }

    throw new Error(JSON.stringify({status: 404, message: "Not supported route"}))
};

const axios = ({method, url, data}) => new Promise((resolve, reject) => {
    try {
        const resolver = getResolver({method, url, data});
        resolve(resolver())
    } catch (err) {
        reject(err);
    }

});
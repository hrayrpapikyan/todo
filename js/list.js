const getToDos = async () => {
    try {

        const data = (await axios({
            method: 'get',
            url: '/'
        })).map(({id, title, description, creator, parent_task_id}) => [
            `
                <div class="form-check">
                    <input class="form-check-input to-do-check" type="checkbox" value="${id}" id="defaultCheck1">
                </div>`,
            title,
            description,
            creator,
            (parent_task_id ? `<a href="${serverUri}/toDo/${parent_task_id}">task 2</a>` : '')

        ]);
        $('#toDoTable').DataTable({
            bFilter: true,
            bInfo: true,
            bServerSide: false,
            bProcessing: false,
            bPaginate: true,
            bAutoWidth: false,
            bStateSave: true,
            iDisplayLength: 25,
            sAjaxSource: null,
            aaData: data,
            aaSorting: [
                [1, "asc"]
            ],
            "aoColumns": [
                {
                    name: "checkbox",
                    sortable: false,
                    searchable: false
                },
                {
                    name: "title",

                },
                {
                    name: "description",
                },
                {
                    name: "creator",
                },
                {
                    name: "parent",
                }
            ]
        });
    } catch (err) {
        console.log(err);
    }
};

const fillParentTaskSelect = async () => {
    const data = await axios({
        method: 'get',
        url: '/'
    });

    const optionsAsString = data.reduce((accumulator, {title, id}) => accumulator + `<option value="${id}">${title}</option>`,
        '<option selected value="0">No Parent</option>');
    $('#parent-tasks').html(optionsAsString);
};

$(function () {
    getToDos();
    fillParentTaskSelect();

    $('#add-to-do').click(function () {
        $('#edit-modal').modal('show');
    });

    $('#save-to-do').click(async function (event) {
        event.preventDefault();

        const data = {
            title: $('#title').val(),
            description: $('#description').val(),
            parent_task_id: $('#parent-tasks').val()
        };

        await axios({
            method: 'post',
            url: '/',
            data
        });


        location.reload();

    });

    $('#toDoTable').on('change', '.to-do-check', function () {
        const isAnyCheckBoxChecked = $('.to-do-check:checked').length;

        if (isAnyCheckBoxChecked) {
            $('#delete-selected').removeClass('disabled');
        } else {
            $('#delete-selected').addClass('disabled');

        }
    });

    $('#delete-selected').click(async function () {
        if ($(this).hasClass('disabled')) {
            return;
        }

        const selectedIds = [];

        $('.to-do-check:checked').each(function () {
            selectedIds.push(parseInt($(this).val()))
        });

        console.log({selectedIds});

        await axios({
            method: 'delete',
            url: '/',
            data: {
                selectedIds
            }
        });


        location.reload();
    })

});
localStorage.setItem('toDos', JSON.stringify([
    {
        id: 1,
        title: 'task 1',
        description: 'description 1',
        creator: 'user 1',
        parent_task_id: 2,
    },
    {
        id: 2,
        title: 'task 2',
        description: 'description 2',
        creator: 'user 1',
        parent_task_id: null,
    },
]));